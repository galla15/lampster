#pragma once
#ifndef LOGGER_H
#define LOGGER_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum{
    INFO,
    DEBUG,
    WARNING,
    ERROR,
    FATAL,
    OFF
}LogLevel;

void __LOG(LogLevel tag, const char* fstr, int lnum, const char* funcName, const char* fmt, ...);
/**
 * @brief Log function using stdout, if a default log file is set the message will also be written there
 * 
 * @param tag log level : enum LogLevel
 * @param FMT message to be written : const char *
 * 
 * @return nothing
 */
#define LOG(tag, FMT, ...) __LOG(tag, __FILE__, __LINE__, __func__, FMT, ##__VA_ARGS__)

void __FLOG(LogLevel tag, const char* file, const char* fstr, int lnum, const char * funcName, const char* fmt, ...);
/**
 * @brief Log function to a different file than the default one
 * 
 * @param tag log level : enum logLevel
 * @param file file name : const char *
 * @param FMT message to be writte : const char *
 * 
 * @return nothing
 */
#define FLOG(tag, file, FMT, ...) __FLOG(tag, file, __FILE__, __LINE__, __func__, FMT, ##__VA_ARGS__)

/**
 * @brief Sets the log level
 * 
 * @param tag log level : enum LogLevel
 */
void logger_setConsoleLogLevel(LogLevel tag);

/**
 * @brief Gets the log level
 * 
 * @return log level : int
 */
LogLevel logger_getConsoleLogLevel(void);

/**
 * @brief Sets a default log file
 * 
 * @param f file name : const char *
 * 
 * @return -1 if an error occured
 */
int logger_setDefaultFile(const char* f);

#ifdef __cplusplus
}
#endif

#endif //LOGGER_H