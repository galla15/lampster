#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <sys/time.h>
#include "logger.h"

#define MICROS 1000000

static LogLevel consoleLvl = INFO;
static const char* defaultLogFile = NULL;
static int defaultFileFlag = 0;
static int logger_init = 0;
static struct timeval tv_init;

static const char levelStr[5][10] =  {{"INFO"}, {"DEBUG"}, {"WARNING"}, {"ERROR"}, {"FATAL"}};

double diff_time(struct timeval tv_start, struct timeval tv_end)
{
    double t_start;
    double t_end;

    t_start = (double)tv_start.tv_sec + (double)tv_start.tv_usec / MICROS;
    t_end = (double)tv_end.tv_sec + (double)tv_end.tv_usec / MICROS;

    return t_end - t_start;
}

void logger_setConsoleLogLevel(LogLevel tag)
{
    consoleLvl = tag;
}

LogLevel logger_getConsoleLogLevel(void)
{
    return consoleLvl;
}

int logger_setDefaultFile(const char* f)
{
    int ret = 1;

    if(defaultFileFlag == 0)
    {
        defaultLogFile = f;
        defaultFileFlag++;

        FILE* file = fopen(defaultLogFile, "w");
        if(file == NULL)
        {
            LOG(ERROR, strerror(errno));
            ret = -1;
        }
        else
        {
            fclose(file);
            defaultFileFlag = 1;
        } 
    }
    else 
    {
        LOG(WARNING, "Default logger file already set");
        ret = -1;
    }
    return ret;
}

void flog(LogLevel tag, const char* file, const char* fstr, int lnum, const char * funcName, const char* fmt, va_list ap)
{
    FILE *f;

    f = fopen(file, "a+");

    if(file != NULL)
    {
        fprintf(f, "(file: %s, func: %s, line: %d)[%s] : ", fstr, funcName, lnum, levelStr[tag]);
        vfprintf(f, fmt, ap);
        fprintf(f, "\n");
    }
    else
    {
        LOG(ERROR, strerror(errno));
        return;
    }

    if(fclose(f) != 0)
    {
        LOG(ERROR, strerror(errno));
        return;
    }

    if(tag == FATAL) exit(EXIT_FAILURE);
}

void __LOG(LogLevel tag, const char* fstr, int lnum, const char* funcName, const char* fmt, ...)
{
    struct timeval tv;
    if(!logger_init)
    {
        gettimeofday(&tv_init, NULL);
        logger_init = 1;
    }
    
    gettimeofday(&tv, NULL);

    va_list ap;
    va_start(ap, fmt);

    if(tag >= consoleLvl)
    {
        printf("[%lfs %s]\t: ", diff_time(tv_init, tv), levelStr[tag]);
        vprintf(fmt, ap);
        printf("\n");
    }
    if(defaultLogFile != NULL)
    {
        va_start(ap, fmt);
        flog(tag, defaultLogFile, fstr, lnum, funcName, fmt, ap);
    }

    if(tag == FATAL) exit(EXIT_FAILURE);

    va_end(ap);
}

void __FLOG(LogLevel tag, const char* file, const char* fstr, int lnum, const char * funcName, const char* fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);

    flog(tag, file, fstr, lnum, funcName, fmt, ap);

    va_end(ap);
}