#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <gattlib.h>
#include <pthread.h>
#include <sys/queue.h>
#include <unistd.h>
#include <signal.h>

#include "BLEbase.h"
#include "logger.h"

#define BLE_SCAN_TIMEOUT 4
#define MILLIS 1000

typedef void (*ble_discovered_device_t)(const char* addr, const char* name);

static pthread_mutex_t g_mutex = PTHREAD_MUTEX_INITIALIZER;

//List for scans
struct ble_dev
{
    char *name;
    char *addr;

    LIST_ENTRY(ble_dev) next;
};
LIST_HEAD(list_head, ble_dev) ble_dev_list;

//Global 
gatt_connection_t* gatt_connection = NULL;
static int connected = 0;
static int numOfDevs = 0;
static int bleInitilized = 0;

static void sig_handler(int sig)
{
    LOG(INFO, "Catched ctrl-c, unlocking mutex");

    if(connected) ble_disconnect(NULL);

    if(sig == SIGINT)
    {
        pthread_mutex_unlock(&g_mutex);
    }
    exit(1);
}

void ble_init()
{
    const char* logfile = "BLEbase.log";
    char initMsg[100];
    logger_setDefaultFile(logfile);

    LOG(INFO, "Logger initialized with output file : %s", logfile);

    signal(SIGINT, sig_handler);

    bleInitilized = 1;
}

int isInit()
{
    if(!bleInitilized)
    {
        LOG(ERROR, "Please call func ble_init() before doing anything else");
        return BLEBASE_ERROR_NOT_INITIALIZED;
    }
    else return BLEBASE_SUCCESS;
}

void delete_list()
{
    LOG(INFO, "Deleting list");
    if(!LIST_EMPTY(&ble_dev_list))
    {
        while(ble_dev_list.lh_first != NULL)
        {
            struct ble_dev* rem = ble_dev_list.lh_first;
            LIST_REMOVE(rem, next);
            free(rem->addr);
            free(rem->name);
            free(rem);
        }
    }
    LOG(INFO, "List deleted");
}

struct ble_dev* search_list(const char* byName)
{
    char str[100];
    struct ble_dev *found = NULL; 

    LOG(INFO, "Searching for device %s", byName);

    if(!LIST_EMPTY(&ble_dev_list))
    {
        struct ble_dev* sdev = ble_dev_list.lh_first;
        while(sdev != NULL)
        {
            if(strcmp(byName, sdev->name) == 0)
            { 
                found = sdev;
                break;
            }
            sdev = LIST_NEXT(sdev, next);
        }
    }

    return found;
}

static void discovered_device_cb(void *adapter, const char *addr, const char *name, void *user_data)
{
    char data[100];
    struct ble_dev *newDev;

    LOG(INFO, "New device found");

    newDev = (struct ble_dev*)malloc(sizeof(struct ble_dev));
    if(newDev == NULL)
    {
        LOG(ERROR, strerror(errno));
        return;
    }

    newDev->addr =strdup(addr);

    if(name) 
    {
        newDev->name = strdup(name);
        LOG(INFO, "%s : %s", name, addr);
    }
    else
    {
        newDev->name = strdup("n/a");
        LOG(INFO, "n/a : %s", addr);
    }
    
    LIST_INSERT_HEAD(&ble_dev_list, newDev, next);
}

int ble_scan(int logActive)
{
    void *adapter;
    int ret = GATTLIB_SUCCESS;
    LogLevel logLvl;

    if(logActive > 0)
    {
        logLvl = logger_getConsoleLogLevel();
        logger_setConsoleLogLevel(INFO);
    }
    else
    {
        logLvl = logger_getConsoleLogLevel();
        logger_setConsoleLogLevel(OFF);
    }
     

    if(isInit() < 0)
    {
        return BLEBASE_ERROR_NOT_INITIALIZED;
    }

    delete_list();

    LIST_INIT(&ble_dev_list);

    LOG(INFO, "Opening adapter");

    ret = gattlib_adapter_open(NULL, &adapter);
    if(ret > 0)
    {
        LOG(ERROR, "Failed to open adapter");
        return BLEBASE_ERROR_SCAN;
    }

    LOG(INFO, "Adapter opened successfully");

    LOG(INFO, "Starting scan...");
    pthread_mutex_lock(&g_mutex);

    ret = gattlib_adapter_scan_enable(adapter, discovered_device_cb, BLE_SCAN_TIMEOUT, NULL);
    if (ret > 0) 
    {
		LOG(ERROR, "Scan failed");
        return BLEBASE_ERROR_SCAN;
	}

	gattlib_adapter_scan_disable(adapter);

	LOG(INFO,"Scan completed");
	pthread_mutex_unlock(&g_mutex);

    ret = gattlib_adapter_close(adapter);
    if(ret != GATTLIB_SUCCESS)
    {
        LOG(WARNING, "Adapter not correctly closed");
    }
    else
    {
        LOG(INFO, "Adapter closed successfully");
    }

    logger_setConsoleLogLevel(logLvl);

    return BLEBASE_SUCCESS;
}

int ble_search_device(const char* name, ble_device* dev)
{
    struct ble_dev* sdev;
    char str[100];

    if(ble_scan(0) != BLEBASE_SUCCESS)
    {
        return BLEBASE_ERROR_SCAN;
    }

    sdev = search_list(name);

    if(sdev == NULL)
    {   
        LOG(INFO, "Device %s not found!", name);
        return BLEBASE_ERROR_DEVICE_NOT_FOUND;
    }

    strcpy(dev->name, sdev->name);
    strcpy(dev->mac_addr, sdev->addr);

    return BLEBASE_SUCCESS;
}

int ble_connect(ble_device* dev)
{
    char str[100];
    int ret;

    if(dev == NULL)
    {
        LOG(ERROR, "Argument <ble_device> dev passed to this function cannot be NULL");
        return BLEBASE_ERROR_CONNECTION;
    }
    
    if(strlen(dev->mac_addr) < 1)
    {
        LOG(ERROR, "Parameter <mac_addr> of argument <ble_device> dev has to be defined");
        return BLEBASE_ERROR_CONNECTION;
    }

    if(connected == 1)
    {
        LOG(ERROR, "You are already connected to a device");
        return BLEBASE_ERROR_CONNECTION;
    }

    pthread_mutex_lock(&g_mutex);
    gatt_connection = gattlib_connect(NULL, dev->mac_addr, GATTLIB_CONNECTION_OPTIONS_LEGACY_DEFAULT);
    pthread_mutex_unlock(&g_mutex);

    if(gatt_connection == NULL)
    {
        LOG(ERROR, "Failed to connect to device %s", dev->mac_addr);
        return BLEBASE_ERROR_CONNECTION;
    }
    else
    {
        connected = 1;
    }
    
    LOG(INFO, "Connected succsessfully to device %s", dev->mac_addr);

    return BLEBASE_SUCCESS;
}

int ble_isConnected()
{
    return connected;
}

int ble_disconnect(ble_device* dev)
{
    char str[100];

    if(gatt_connection == NULL)
    {
        LOG(WARNING, "You are not connected to any device. Please connect before trying to disconnect");
        return BLEBASE_ERROR_CONNECTION;
    }

    pthread_mutex_lock(&g_mutex);
    while (gattlib_disconnect(gatt_connection) != GATTLIB_SUCCESS)
    {
        usleep(100 * MILLIS);
        LOG(INFO, "Trying to disconnect...");
    }
    pthread_mutex_unlock(&g_mutex);

    connected = 0;

    if(dev == NULL)
    {
        LOG(INFO, "Disconnected successfully");
    }
    else
    {
        LOG(INFO, "Disconnected successfully from %s", dev->mac_addr);
    }

    return BLEBASE_SUCCESS;
}

char* clean_buffer(char* str)
{
    char *sub = "0x";
    size_t len = strlen(sub);
    if (len > 0) {
        char *p = str;
        while ((p = strstr(p, sub)) != NULL) {
            memmove(p, p + len, strlen(p + len) + 1);
        }
    }
    return str;
}

int ble_write_char(uint16_t handle, char* buffer)
{
    int ret; 
    uint16_t data;
    size_t datasize;
    char *cleaned_buf = clean_buffer((char*)buffer);

    datasize = strlen(cleaned_buf) / 2;
    data = strtoul(buffer, NULL, 16);

    if(!ble_isConnected())
    {
        LOG(ERROR, "You must be connected to write to a device");
        return BLEBASE_ERROR_CONNECTION;
    }

    pthread_mutex_lock(&g_mutex);
    ret = gattlib_write_char_by_handle(gatt_connection, handle, &data, datasize);
    //ret = gattlib_write_without_response_char_by_uuid(gatt_connection, &uuid, &data, sizeof(data));
    pthread_mutex_unlock(&g_mutex);

    if(ret != GATTLIB_SUCCESS)
    {
        if(ret == GATTLIB_NOT_FOUND)
        {
            LOG(ERROR, "Could not find GATT Characteristic with handle 0x%x", handle);
            return BLEBASE_ERROR_WRITE;
        }
        else
        {
            LOG(ERROR, "Error while writing GATT Characteristic with handle 0x%x (ret:%d)", handle, ret);
            return BLEBASE_ERROR_WRITE;
        }
        
    }

    LOG(INFO, "Succefully written to device");

    return BLEBASE_SUCCESS;
}