#pragma once
#ifndef BTBASE_H
#define BTBASE_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Struct of to store BLE devices
 * 
 * @param name name of the device : char[250]
 * @param mac_addr MAC address of the device : char[19]
 */
typedef struct {
    char name[250];
    char mac_addr[19];
}ble_device;

/**
 * @brief BLEBase errors
 */ 
//@{
#define BLEBASE_SUCCESS 1
#define BLEBASE_ERROR_NOT_INITIALIZED -1
#define BLEBASE_ERROR_SCAN -2
#define BLEBASE_ERROR_DEVICE_NOT_FOUND -3
#define BLEBASE_ERROR_CONNECTION -4
#define BLEBASE_ERROR_WRITE -5 
#define BLEBASE_ERROR_WRITE_HANDLE -6
//@}

/**
 * @brief Initialize all things needed by BLEbase. Must be called before anything else
 * 
 * @return nothing
 */
void ble_init(void);

/**
 * @brief Launches a scan for BLE devices
 * 
 * @param logActive activate console log : int (0 off, 1 on)
 * 
 * @return BLEBASE_SUCCESS or BLEBASE_ERROR_*
 */
int ble_scan(int logActive);

/**
 * @brief Searches for a specific device by name
 * 
 * @param name the name of the searched device : const char*
 * @param dev a struct to be modified if a device is found else setted to NULL. Must be allocated by the user : ble_device*
 * 
 * @return BLEBASE_SUCCESS or BLEBASE_ERROR_*
 */
int ble_search_device(const char* name, ble_device* dev);

/**
 * @brief Check the connection state
 * 
 * @return 1 (true) ot 0 (false)
 */
int ble_isConnected(void);

/**
 * @brief Tries to connect to a BLE device
 * 
 * @param dev device to connect to : ble_device*
 * 
 * @return BLEBASE_SUCCESS or BLEBASE_ERROR_*
 */
int ble_connect(ble_device* dev);

int ble_disconnect(ble_device* dev);

int ble_write_char(uint16_t handle, char* buffer);

int BLEBase_foundDeviceNum();

#ifdef __cplusplus
}
#endif

#endif //BTBASE_H