cmake_minimum_required(VERSION 3.0.0)

add_library(BLEbase BLEbase.c BLEbase.h logger.c logger.h)

find_package(PkgConfig REQUIRED)
pkg_search_module(GLIB REQUIRED glib-2.0)
pkg_search_module(GATTLIB REQUIRED gattlib)
target_include_directories(BLEbase PRIVATE ${GLIB_INCLUDE_DIRS} ${GATTLIB_INCLUDE_DIRS})

#Adding libs to be linked
set(EXTRA_LIBS "bluetooth" )

target_link_libraries(BLEbase ${EXTRA_LIBS} ${GLIB_LDFLAGS} ${GATTLIB_LDFLAGS})
