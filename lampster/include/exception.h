#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <string>

namespace Lampster
{
    namespace Exception
    {
        struct error{
            int id;
            std::string msg;
        };

        class Exception : public std::exception
        {
            private :
                std::string m_error;
            protected :
                Exception(std::string error) : m_error(error) {}
                static std::string name(int id, const std::string& _name)
                {
                    return "[lampster.exception." + _name + "." + std::to_string(id) + "]";
                }
            public:
                const char* what() const noexcept {return m_error.c_str(); }

        };

        class connection_error : public Exception
        {
            private:
                int id = 100;
                connection_error(const char* error) : Exception(error) {}
            public:
                static connection_error create(error err);

                const static error not_connected;
                const static error failed;
        };

        class search_error : public Exception
        {
            private:
                search_error(const char* error) : Exception(error) {}
            public:
                static search_error create(error err);

                const static error no_device_found;
        };
    }
}

#endif //EXCEPTION_H