#pragma once
#ifndef LAMPSTER_H
#define LAMPSTER_H

#define TRIES 10

#include <string>

namespace Lampster
{ 
    struct cmd_t
    {
        uint16_t handle;
        std::string char_val;
    };

    class Lampster
    {
    private:
        std::string config = "config.json";
        std::string addr;
        std::string name;
        bool isOn = false;
        bool connected = false;

        cmd_t turn_on_cmd;
        cmd_t turn_off_cmd;
        cmd_t white_cmd;

        int write_char(cmd_t cmd);

    public:
        Lampster(/* args */);
        ~Lampster();

        void scan();
        int search(std::string dev_name = "");
        int connect(std::string mac_addr = "");
        int turn_on();
        int turn_off();
        int disconnect();
        bool is_ON();
        int coldWhite(int val);
        int warmWhite(int val);

        void print();
    };
} //namespace

#endif //LAMPSTER_H