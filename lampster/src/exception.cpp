#include "exception.h"

namespace Lampster
{
    namespace Exception
    {
        //connection error implementation
        const error connection_error::not_connected =  {101, "Device not connected"};
        const error connection_error::failed = {102, "Connection to device failed"};

        connection_error connection_error::create(error err)
        {
            std::string w = Exception::name(err.id , "connection_error") + " : " + err.msg;
            return connection_error(w.c_str());
        }

        //scan error implementation
        const error search_error::no_device_found {201, "No device found"};

        search_error search_error::create(error err)
        {
            std::string w = Exception::name(err.id, "scan_error") + " : " + err.msg;
            return search_error(w.c_str());
        }

    }
}