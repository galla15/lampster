#include "lampster.h"
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <cstdint>
#include <fstream>
#include "logger.h"
#include "BLEbase.h"
#include "json.hpp"
#include "exception.h"

#define MILLIS 1000

using namespace std;
using json = nlohmann::json;

namespace Lampster
{

	void init_cmd(json data, cmd_t &cmd, string scmd)
	{
		string handle;
		string char_value;
		uint16_t hex_handle;

		try
		{
			handle = data["handles"][scmd];
		}
		catch(json::type_error& e)
		{
			LOG(ERROR, "%s", e.what());
			exit(1);
		}

		hex_handle = (uint16_t)stoul(handle, nullptr, 16);

		try
		{
			char_value = data["init_char"][scmd];
		}
		catch(json::type_error& e)
		{
			LOG(ERROR, "%s", e.what());
			exit(1);
		}

		cmd = {hex_handle, char_value};
	}

	Lampster::Lampster(/* args */)
	{
		//logger_setConsoleLogLevel(OFF);
		std::ifstream jFile;
		json data;
		jFile.open(config);

		ble_init();

		try
		{
			data = json::parse(jFile);
		}
		catch(json::parse_error& e)
		{
			LOG(ERROR, e.what());
			exit(1);
		}

		addr = data["mac"];
		name = data["name"];

		init_cmd(data, turn_on_cmd, "on");
		init_cmd(data, turn_off_cmd, "off");
		init_cmd(data, white_cmd, "white");

		LOG(INFO, "Lampster initialized");
	}

	Lampster::~Lampster()
	{
		disconnect();
	}

	int Lampster::search(string dev_name)
	{
		ble_device *dev = (ble_device*)malloc(sizeof(ble_device));
		int ret;

		if(dev_name.empty()) dev_name = this->name;

		ret = ble_search_device(dev_name.c_str(), dev);

		if(ret < 0)
		{
			throw Exception::search_error::create(Exception::search_error::no_device_found);
		}
		LOG(INFO, "Device %s found", name.c_str());
		return 0;
	}

	int Lampster::connect(string mac_addr)
	{
		ble_device *dev = (ble_device*)malloc(sizeof(ble_device));
		int ret, tries = TRIES;

		search();

		if(!mac_addr.empty())
		{
			strcpy(dev->mac_addr, mac_addr.c_str());
		}
		else
		{
			strcpy(dev->mac_addr, addr.c_str());
		}

		do
		{
			LOG(INFO, "Attempting to connect... %d", (TRIES - tries) + 1);
			ret = ble_connect(dev);
			tries--;
			//usleep(100000);
		}while(ret != BLEBASE_SUCCESS && tries > 0);
			
		if(ret != BLEBASE_SUCCESS)
		{
			throw Exception::connection_error::create(Exception::connection_error::failed);
		}

		connected = true;
		
		return ret;
	}

	int Lampster::write_char(cmd_t cmd)
	{
		int ret;
		int tries = TRIES;

		LOG(INFO, "Writing characteristic");
		LOG(DEBUG, "Characteristic : handle : 0x%x, value : %s", turn_on_cmd.handle, turn_on_cmd.char_val.c_str());

		if(!connected)
		{
			throw Exception::connection_error::create(Exception::connection_error::not_connected);
		}

		do
		{
			ret = ble_write_char(cmd.handle, &cmd.char_val[0]);
			tries--;
			usleep(100*MILLIS);
		} while (ret == BLEBASE_ERROR_WRITE && tries > 0);

		return ret;
	}

	int Lampster::turn_on()
	{
		LOG(INFO, "Sending turn on command");

		if(write_char(turn_on_cmd))
		{
			isOn = true;
			return 1;
		}
		else
		{
			return -1;
		}
	}

	int Lampster::turn_off()
	{
		if(write_char(turn_off_cmd))
		{
			isOn = false;
			return 1;
		}
		else
		{
			return -1;
		}
	}

	std::string int_to_hex(int val)
	{
		char buf[20];
		sprintf(buf, "%x", val);

		string str(buf);
		return str;
	}

	int Lampster::coldWhite(int val)
	{
		string valstr = int_to_hex(val);
		white_cmd.char_val.replace(2, 2, valstr,0,2);

		if(write_char(white_cmd))
		{
			isOn = true;
			usleep(200*MILLIS);
			return 1;
		}
		else
		{
			usleep(200*MILLIS);
			return -1;
		}
	}

	int Lampster::disconnect()
	{
		if(ble_isConnected()) return ble_disconnect(NULL);
		else return -1;
	}

	void Lampster::scan()
	{	
		ble_scan(1);	
	}

	bool Lampster::is_ON()
	{
		return isOn;
	}

	void Lampster::print()
	{
		cout << "name : " << this->name << "\n addr : " << this->addr << endl;
	}

} //Namespace