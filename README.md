# Lampster

    This is a work in progress to develop a cpp backend to control a Lampster Lamp
    This project will then be integrated in a smarthome rest server

## Install
    clone it using : git clone --recursive https://...  cause there are some submodules to get.
    install libbluetooth-dev : sudo apt install libbluetooth-dev
    install bluez : sudo apt install bluez
    install glib2.0 : sudo apt install libglib2.0-dev
    then use cmake to build the project

## Troubleshooting
    If nothing is working anymore try restarting the bluetooth service.
    Else reboot your machine (if working with a vm reboot the host machine as well)

## Thanks
Thanks to Noki for reverse engeeniring the Lampster packet\
[Noki's project](https://www.github.com/Noki/the-lampster)
