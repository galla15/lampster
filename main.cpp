#include <iostream>
#include <unistd.h>
#include "lampster.h"


int main(int, char**) {
    
    Lampster::Lampster lampster;

    //lampster.scan();
    //lampster.search();

    lampster.connect();
    
    lampster.turn_on();

    lampster.coldWhite(50);

    sleep(5);

    lampster.turn_off();

    lampster.disconnect();

}
